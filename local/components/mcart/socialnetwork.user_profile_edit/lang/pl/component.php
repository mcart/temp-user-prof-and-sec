<?
$MESS["BLOG_MODULE_NOT_INSTALL"] = "Moduł sieci społecznej nie został zainstalowany.";
$MESS["SONET_P_USER_NO_USER"] = "Użytkownik nie został znaleziony.";
$MESS["SONET_P_PU_NO_RIGHTS"] = "Niedostateczne uprawnienia do edycji profilu.";
$MESS["SONET_P_USER_SEX_M"] = "Mężczyzna";
$MESS["SONET_P_USER_SEX_F"] = "Kobieta";
$MESS["SONET_P_USER_TITLE"] = "Edycja profilu użytkownika";
$MESS["SONET_P_USER_TITLE_VIEW"] = "Edytuj profil";
?>