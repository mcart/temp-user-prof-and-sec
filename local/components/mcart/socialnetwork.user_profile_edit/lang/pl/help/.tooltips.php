<?
$MESS["PATH_TO_USER_TIP"] = "Ścieżka do strony profilu użytkownika. Przykład: sonet_user.php?page=user&user_id=#user_id#.";
$MESS["PATH_TO_USER_EDIT_TIP"] = "Ścieżka do edytora profilu użytkownika strony. Przykład: sonet_user_edit.php?page=user&user_id=#user_id#&mode=edit.";
$MESS["PAGE_VAR_TIP"] = "Określić tutaj nazwę zmiennej, dla której strona sieci społeczna zostanie przyjęta.";
$MESS["USER_VAR_TIP"] = "Określić tutaj nazwę zmiennej, dla której ID użytkownika sieci społecznej zostaną przyjęte.";
$MESS["ID_TIP"] = "Określa kod, którego wynikiem jest identyfikator użytkownika.";
$MESS["SET_TITLE_TIP"] = "Zaznaczenie tej opcji spowoduje ustawienie tytułu strony dla <i>\"nazwy użytkownika\"</i> <b>Profilu użytkownika</b>.";
$MESS["USER_PROPERTY_TIP"] = "Wybierz tutaj dodatkowe właściwości, które będą wyświetlane w profilu użytkownika.";
?>