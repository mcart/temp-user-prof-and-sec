<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arResult['USER_PROP'] = array();

$arRes = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);
if (!empty($arRes))
{
	foreach ($arRes as $key => $val)
	{
		$arResult['USER_PROP'][$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? htmlspecialcharsbx($val["EDIT_FORM_LABEL"]) : $val["FIELD_NAME"]);

		$val['ENTITY_VALUE_ID'] = $arResult['User']['ID'];

		$val['VALUE'] = $arResult['User']["~".$val['FIELD_NAME']];
		$arResult['USER_PROPERTY_ALL'][$val['FIELD_NAME']] = $val;
	}
}


$arGroupCurUser = CUser::GetUserGroup($USER->GetID());
$flagUserGod = false;
if(!empty($arParams['GOD_GROUPS'])) {
    foreach ($arGroupCurUser as $id) {
        if (in_array($id, $arParams['GOD_GROUPS'])) {
            $flagUserGod = true;
            break;
        }
    }
}
//echo $flagUserGod;
if (empty($arParams['USER_FIELDS_BAN']))
    $arParams['USER_FIELDS_BAN'] = array();

if (empty($arParams['USER_PROPERTY_BAN']))
    $arParams['USER_PROPERTY_BAN'] = array();

foreach ($arParams['EDITABLE_FIELDS'] as $key => $field){
    if((in_array($field, $arParams['USER_FIELDS_BAN']) || in_array($field, $arParams['USER_PROPERTY_BAN'])) && ($flagUserGod == false)){
        unset($arParams['EDITABLE_FIELDS'][$key]);
    }
}

//echo "<pre>";
//print_r($arResult);
//echo "</pre>";

//foreach ($arResult['User'] as $key => $field){
//    if(){

//    }
//}

?>