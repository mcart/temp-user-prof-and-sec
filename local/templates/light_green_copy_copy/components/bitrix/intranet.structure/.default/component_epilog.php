<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!empty($arResult['NAME']) && !empty($arResult['UF_ABBREVIATION']))
{
    $APPLICATION->SetTitle($arResult['NAME']." (".$arResult['UF_ABBREVIATION'].")");
}