<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
__IncludeLang(dirname(__FILE__)."/lang/".LANGUAGE_ID."/result_modifier.php");
if (!$arResult["CurrentUserPerms"]["Operations"]["viewprofile"])
{
	$arResult["FatalError"] = GetMessage("SONET_C39_USER_ACCESS_DENIED");
}
//echo "fsdfsdfsdf";

$arExIDs = array();
$seIDs = array();
$rsSectMain = CIBlockSection::GetList(array('left_margin' => 'asc'), array("NAME" => "Locations", "IBLOCK_ID" => 5 ), false, array());
while ($arSectMain = $rsSectMain->GetNext())
{
    $sectMain = $arSectMain;
    $arExIDs[] = $arSectMain["ID"];
}

$arFilter = array('IBLOCK_ID' => 5,'>LEFT_MARGIN' => $sectMain['LEFT_MARGIN'],'<RIGHT_MARGIN' => $sectMain['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $sectMain['DEPTH_LEVEL']);
$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, array("UF_HEAD", "ID"));
while ($arSect = $rsSect->GetNext())
{
    $arExIDs[] = $arSect["ID"];
}


foreach ($arResult["User"]["UF_DEPARTMENT"] as $key => $dep){
    if(in_array($dep, $arExIDs)){
        unset($arResult["User"]["UF_DEPARTMENT"][$key]);
    }
}

$arResult['MANAGERS'] = CIntranetUtils::GetDepartmentManager($arResult["User"]["UF_DEPARTMENT"], $arResult["User"]["ID"], true);

$arGroupCurUser = CUser::GetUserGroup($USER->GetID());
$flagUserGod = false;
if(!empty($arParams['GOD_GROUPS'])){
	foreach ($arGroupCurUser as $id){
		if(in_array($id, $arParams['GOD_GROUPS'])){
			$flagUserGod = true;
			break;
		}
	}
}
if($arResult["CurrentUserPerms"]["Operations"]["modifyuser"] == 0){
    $arResult["CurrentUserPerms"]["Operations"]["modifyuser"] = $flagUserGod;
}
if($arResult["CurrentUserPerms"]["Operations"]["modifyuser_main"] == 0){
    $arResult["CurrentUserPerms"]["Operations"]["modifyuser_main"] = $flagUserGod;
}
?>