<?
$MESS["BPABL_TYPE_5"] = "Akcja '#ACTIVITY#'#NOTE#";
$MESS["BPABL_TYPE_4"] = "Błąd akcji '#ACTIVITY#'#NOTE#";
$MESS["BPABL_STATUS_3"] = "Zostanie odwołana";
$MESS["BPABL_STATE_NAME"] = "Aktualny stan procesów biznesowych";
$MESS["BPABL_LOG"] = "Historia procesów biznesowych";
$MESS["BPABL_RES_3"] = "Anulowanie";
$MESS["BPABL_TYPE_3"] = "Anulowano akcję '#ACTIVITY#'#NOTE#";
$MESS["BPABL_STATUS_4"] = "Zakończono";
$MESS["BPABL_TYPE_2"] = "Zakończono akcję '#ACTIVITY#'	 zwróć status: '#STATUS#'	 wynik: '#RESULT#'#NOTE#";
$MESS["BPABL_STATE_MODIFIED"] = "Data aktualnego stanu";
$MESS["BPABL_STATUS_5"] = "Błąd";
$MESS["BPABL_RES_4"] = "Błąd";
$MESS["BPABL_STATUS_2"] = "W trakcie realizacji";
$MESS["BPABL_STATUS_1"] = "Zainicjowany";
$MESS["BPABL_TYPE_1"] = "Rozpoczęto akcję '#ACTIVITY#'#NOTE#";
$MESS["BPABL_RES_1"] = "Nie";
$MESS["BPABL_RES_5"] = "Nie zainicjowany";
$MESS["BPABL_TYPE_6"] = "Nieznana akcja związana z'#ACTIVITY#'#NOTE# została przeprowadzona";
$MESS["BPABL_RES_2"] = "Sukces";
$MESS["BPABL_STATUS_6"] = "Niezdefioniowano";
$MESS["BPABL_RES_6"] = "Niezdefioniowano";
$MESS["BPWC_WLCT_TOTAL"] = "Ogółem";
?>