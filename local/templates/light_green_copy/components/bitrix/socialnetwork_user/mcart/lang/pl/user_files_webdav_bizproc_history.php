<?
$MESS["WD_ACCESS_DENIED"] = "Odmowa dostępu.";
$MESS["WD_VERSIONS"] = "Wersje";
$MESS["WD_VERSIONS_ALT"] = "Otwórz wersje dokumentu";
$MESS["WD_ORIGINAL"] = "Oryginał";
$MESS["WD_EDIT_FILE"] = "Edytuj";
$MESS["WD_EDIT_FILE_ALT"] = "Edytuj dokument";
$MESS["WD_EDIT_FILE_ALT2"] = "Edytuj wersje dokumentu";
$MESS["WD_HISTORY_DOCUMENT_TITLE"] = "Historia dokumentu #NAME#";
$MESS["WD_HISTORY_VERSION_TITLE"] = "Historia wersji #NAME#";
?>