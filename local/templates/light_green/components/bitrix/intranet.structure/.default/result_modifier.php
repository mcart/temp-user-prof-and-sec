<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/*function link_it($text)
{
    $text= preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
    $text= preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" >$3</a>", $text);
    $text= preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
    return($text);
}*/
if($depID = $_GET[$arParams['FILTER_NAME'].'_UF_DEPARTMENT'])
{
    $arFilter = array(
        "IBLOCK_ID" => 5,
        'ID' => $depID
    );
    $rsSect = CIBlockSection::GetList(array(), $arFilter, false, array('UF_ABBREVIATION', 'UF_FUNC_RESP', 'UF_DEP_RESP', 'NAME'));
    while ($arSect = $rsSect->Fetch())
    {
        //echo "<pre>";
        //print_r($arSect);
        //echo "</pre>";

        $arResult['UF_ABBREVIATION'] = $arSect['UF_ABBREVIATION'];
        $arResult['UF_FUNC_RESP'] = $arSect['UF_FUNC_RESP'];
        $text = $arSect['UF_DEP_RESP'];
        $text= preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
        $text= preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" >$3</a>", $text);
        $text= preg_replace("/(^|[\n ])([a-z0-9&\-_\.]+?)@([\w\-]+\.([\w\-\.]+)+)/i", "$1<a href=\"mailto:$2@$3\">$2@$3</a>", $text);
        $arResult['UF_DEP_RESP'] = $text;
        $arResult['NAME'] = $arSect['NAME'];

        if(!empty($arResult['UF_ABBREVIATION']))
        {
            $cp = $this->__component;

            $cp->SetResultCacheKeys(array(
                "NAME",
                "UF_ABBREVIATION"
            ));
        }
    }
}



