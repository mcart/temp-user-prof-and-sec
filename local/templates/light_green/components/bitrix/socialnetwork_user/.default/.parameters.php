<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if (!CModule::IncludeModule("socialnetwork"))
    return false;

$arResBan = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("USER", 0, LANGUAGE_ID);
$userPropBan = array();
if (!empty($arResBan))
{
foreach ($arResBan as $key => $val)
$userPropBan[$val["FIELD_NAME"]] = (strLen($val["EDIT_FORM_LABEL"]) > 0 ? $val["EDIT_FORM_LABEL"] : $val["FIELD_NAME"]);
}
$userProp1Ban = CSocNetUser::GetFields();
unset($userProp1Ban["PASSWORD"]);

$rsGroups = CGroup::GetList($by = "c_sort", $order = "asc", array());
while($arGroups = $rsGroups->Fetch())
{
    $arUsersGroups[$arGroups['ID']] = $arGroups['NAME'];
}



$arTemplateParameters = array(

    "USER_FIELDS_BAN" => array(
        "PARENT" => "ADDITIONAL_SETTINGS",
        "NAME" => GetMessage("USER_FIELDS_BAN"),
        "TYPE" => "LIST",
        "VALUES" => $userProp1Ban,
        "MULTIPLE" => "Y",
        "DEFAULT" => array(),
    ),
    "USER_PROPERTY_BAN" => array(
        "PARENT" => "ADDITIONAL_SETTINGS",
        "NAME" => GetMessage("USER_PROPERTY_BAN"),
        "TYPE" => "LIST",
        "VALUES" => $userPropBan,
        "MULTIPLE" => "Y",
        "DEFAULT" => array(),
    ),
    "GOD_GROUPS" => array(
        "PARENT" => "ADDITIONAL_SETTINGS",
        "NAME" => GetMessage("GOD_GROUPS"),
        "TYPE" => "LIST",
        "VALUES" => $arUsersGroups,
        "MULTIPLE" => "Y",
        "DEFAULT" => array(),
    ),

);